export default {
	name: "cd",
	description: "Changes directory",
	requires: ["mergePath", "fs"],
	run: function(args, nodeprocess, lib) {
		let path = lib.mergePath(nodeprocess.dir, args[0]);
		if(!lib.fs.existsSync(path) || !lib.fs.lstatSync(path).isDirectory()) {
			nodeprocess.stdout.write(`Invalid directory: ${path}\n`);
			return;
		};

		nodeprocess.dir = path;
	}
};