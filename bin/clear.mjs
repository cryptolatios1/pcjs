export default {
	name: "clear",
	description: "Clears screen",
	run: function() {
		console.clear();
	}
};