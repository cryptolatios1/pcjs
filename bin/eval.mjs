export default {
	name: "eval",
	description: "Evaluates code",
	run: function(args, nodeprocess, lib) {
		eval(args.join(" "));
	}
};