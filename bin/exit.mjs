export default {
	name: "exit",
	description: "Stops program",
	run: function(args, nodeprocess) {
		nodeprocess.stdout.write("Bye!\n");
		nodeprocess.exit();
	}
};