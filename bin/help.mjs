export default {
	name: "help",
	description: "Show help",
	run: function(args, nodeprocess) {
		let help = "";
		for(let commandName of Object.keys(global.commands)) {
			help += `\x1b[34m${commandName}\x1b[0m - ${global.commands[commandName].description ?? 'No description'}\n`;
		};
		nodeprocess.stdout.write(help);
	}
};