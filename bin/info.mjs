export default {
	name: "inxi",
	description: "Shows information",
	requires: ["os"],
	run: function(args, nodeprocess, lib) {
		let cpu = lib.os.cpus()[0];
		nodeprocess.stdout.write(`\x1b[34mCPU:\x1b[0m ${cpu.model}\n`);
	}
};