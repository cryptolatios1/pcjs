export default {
	name: "ls",
	description: "list files",
	requires: ["fs"],
	run: function(args, nodeprocess, lib) {
		let list = "";
		let tempdir = args[0] ? lib.mergePath(nodeprocess.dir, args[0]) : nodeprocess.dir;
		if(!lib.fs.existsSync(tempdir) || !lib.fs.lstatSync(tempdir).isDirectory()) {
			nodeprocess.stdout.write(`Invalid directory: ${tempdir}\n`);
			return;
		};

		for(let filename of lib.fs.readdirSync(tempdir)) {
			list += `\x1b[${lib.fs.lstatSync(`${tempdir}/${filename}`).isDirectory() ? "34" : "32"}m${filename}\x1b[0m `;
		};

		nodeprocess.stdout.write(`${list}\n`);
	}
};