export default {
	name: "reload",
	description: "Reloads modules",
	requires: ["fs"],
	run: async function(args, nodeprocess, lib) {
		global.commands = {};
		let tempdir = new URL("..", import.meta.url).pathname;

		for(let filename of lib.fs.readdirSync(`${tempdir}/bin`)) {
			let data = (await import(`${tempdir}/bin/${filename}`)).default;
			global.commands[data.name] = data;
		};
	}
};