export default {
	name: "rm",
	description: "removes files",
	requires: ["mergePath", "fs"],
	run: function(args, nodeprocess, lib) {
		let recursive = args.indexOf("-r");
		if(recursive !== -1) {
			args.splice(recursive, 1)[0];
			recursive = true;
		} else {
			recursive = false;
		};

		let temp = args[0] ? lib.mergePath(nodeprocess.dir, args[0]) : nodeprocess.dir;
		if(!lib.fs.existsSync(temp)) {
			nodeprocess.stdout.write(`Invalid file: ${temp}\n`);
			return;
		};

		if(!lib.fs.lstatSync(temp).isDirectory()) {
			lib.fs.rmSync(temp);
			return;
		};

		if(!recursive) {
			nodeprocess.stdout.write(`Remove folder: Try -r\n`);
			return;
		};

		lib.fs.rmSync(temp, {recursive: true});
	}
};