export default {
	name: "touch",
	description: "creates empty files",
	requires: ["mergePath", "fs"],
	run: function(args, nodeprocess, lib) {
		let temp = args[0] ? lib.mergePath(nodeprocess.dir, args[0]) : nodeprocess.dir;
		if(lib.fs.existsSync(temp)) {
			nodeprocess.stdout.write(`file exists: ${temp}\n`);
			return;
		};

		lib.fs.writeFileSync(temp, "");
	}
};