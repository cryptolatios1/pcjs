export default {
	name: "mergePath",
	description: "merges two paths und resolves them",
	/* base should be absolute like /home/user/ */ 
	value: (base, path) => {
		let pathparts2 = path.trim().split(/\/+/gim);
		if(pathparts2[0] === "") return path; /* Starts with "/" => absolute new path */
		if(pathparts2.at(-1) === "") pathparts2.pop(); /* If ends with "/" => remove from end */

		let mergedparts = base.trim().split(/\/+/gim);
		if(mergedparts.at(-1) === "") mergedparts.pop(); /* if ends with "/" => remove */

		for(let newpath of pathparts2) {
			/* Same folder */
			if(newpath === ".") {

			} else if(newpath === "..") { /* Folder above */
				mergedparts.pop();
			} else {
				mergedparts.push(newpath);
			};
		};
		return mergedparts.join("/");
	}
};