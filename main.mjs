import nodefs from "node:fs";
import readline from "node:readline";
import nodeprocess from "node:process";

global.commands = {};
nodeprocess.dir = new URL(".", import.meta.url).pathname;

let libraries = {};

for(let filename of nodefs.readdirSync("./lib")) {
	let data = (await import(`./lib/${filename}`)).default;
	libraries[data.name] = data.value;
};

for(let filename of nodefs.readdirSync("./bin")) {
	let data = (await import(`./bin/${filename}`)).default;
	global.commands[data.name] = data;
};

console.clear();

readline.createInterface({input: nodeprocess.stdin, output: nodeprocess.stdout}).on("line", function(line) {
	let [command, ...args] = line.trim().split(/ +/gim);
	if(!command) return;

	let commandData = global.commands[command];
	if(!commandData) {
		nodeprocess.stdout.write("Command not found\n");
		return;
	};

	commandData.run(args, nodeprocess, libraries);
});